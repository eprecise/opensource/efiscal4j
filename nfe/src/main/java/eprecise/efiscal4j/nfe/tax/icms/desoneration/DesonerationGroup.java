
package eprecise.efiscal4j.nfe.tax.icms.desoneration;

@DesonerationGroupValidation
public interface DesonerationGroup {

	public String getIcmsDesonerationValue();

	public ICMSDesonerationReason getIcmsDesonerationReason();

}
