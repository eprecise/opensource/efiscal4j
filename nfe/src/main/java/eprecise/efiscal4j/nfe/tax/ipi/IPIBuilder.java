
package eprecise.efiscal4j.nfe.tax.ipi;

public interface IPIBuilder {

    IPI build();
}
